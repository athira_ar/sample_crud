FROM golang:1.16
WORKDIR /root/go_crud
COPY . .
RUN go mod init go_crud
RUN go mod tidy
CMD ["go","run","main.go"]

