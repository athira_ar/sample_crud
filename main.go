package main

import (
    "database/sql"
    "log"
    "net/http"
    "text/template"
    _ "github.com/go-sql-driver/mysql"
)

type Book_details struct {
    Id    int
    Name  string
    Author string
    Price int
}

func dbConn() (db *sql.DB) {
    dbDriver := "mysql"
    dbUser := "username"
    dbPass := "password"
    dbName := "books"
    db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp(db_host:3306)/"+dbName)
    if err != nil {
        panic(err.Error())
    }
    return db
}

var tmpl = template.Must(template.ParseGlob("form/*"))

func Index(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    selDB, err := db.Query("SELECT * FROM book_details ORDER BY id DESC")
    if err != nil {
        panic(err.Error())
    }
    emp := Book_details{}
    res := []Book_details{}
    for selDB.Next() {
        var id, price int
        var name, author string
        err = selDB.Scan(&id, &name, &author, &price)
        if err != nil {
            panic(err.Error())
        }
        emp.Id = id
        emp.Name = name
        emp.Author = author
        emp.Price = price
        res = append(res, emp)
    }
    tmpl.ExecuteTemplate(w, "Index", res)
    defer db.Close()
}

func Show(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    nId := r.URL.Query().Get("id")
    selDB, err := db.Query("SELECT * FROM book_details WHERE id=?", nId)
    if err != nil {
        panic(err.Error())
    }
    emp := Book_details{}
    for selDB.Next() {
        var id, price int
        var name, author string
        err = selDB.Scan(&id, &name, &author, &price)
        if err != nil {
            panic(err.Error())
        }
        emp.Id = id
        emp.Name = name
        emp.Author = author
        emp.Price = price
    }
    tmpl.ExecuteTemplate(w, "Show", emp)
    defer db.Close()
}

func New(w http.ResponseWriter, r *http.Request) {
    tmpl.ExecuteTemplate(w, "New", nil)
}

func Edit(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    nId := r.URL.Query().Get("id")
    selDB, err := db.Query("SELECT * FROM book_details WHERE id=?", nId)
    if err != nil {
        panic(err.Error())
    }
    emp := Book_details{}
    for selDB.Next() {
        var id, price int
        var name, author string
        err = selDB.Scan(&id, &name, &author, &price)
        if err != nil {
            panic(err.Error())
        }
        emp.Id = id
        emp.Name = name
        emp.Author = author
        emp.Price = price
    }
    tmpl.ExecuteTemplate(w, "Edit", emp)
    defer db.Close()
}

func Insert(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    if r.Method == "POST" {
        name := r.FormValue("name")
        author := r.FormValue("author")
        price := r.FormValue("price")
        insForm, err := db.Prepare("INSERT INTO book_details(name, author, price) VALUES(?,?,?)")
        if err != nil {
            panic(err.Error())
        }
        insForm.Exec(name, author, price)
        log.Println("INSERT: Name: " + name + " | Author: " + author + " | Price: " + price)
    }
    defer db.Close()
    http.Redirect(w, r, "/", 301)
}

func Update(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    if r.Method == "POST" {
        name := r.FormValue("name")
        author := r.FormValue("author")
        price := r.FormValue("price")
        id := r.FormValue("uid")
        insForm, err := db.Prepare("UPDATE book_details SET name=?, author=?, price=? WHERE id=?")
        if err != nil {
            panic(err.Error())
        }
        insForm.Exec(name, author, price, id)
        log.Println("UPDATE: Name: " + name + " | author: " + author + " | price: " + price)
    }
    defer db.Close()
    http.Redirect(w, r, "/", 301)
}

func Delete(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    emp := r.URL.Query().Get("id")
    log.Println(emp)
    delForm, err := db.Prepare("DELETE FROM book_details WHERE id=?")
    if err != nil {
        panic(err.Error())
    }
    delForm.Exec(emp)
    log.Println("DELETE")
    defer db.Close()
    http.Redirect(w, r, "/", 301)
}

func main() {
    log.Println("Server started on: http://localhost:8080")
    http.HandleFunc("/", Index)
    http.HandleFunc("/show", Show)
    http.HandleFunc("/new", New)
    http.HandleFunc("/edit", Edit)
    http.HandleFunc("/insert", Insert)
    http.HandleFunc("/update", Update)
    http.HandleFunc("/delete", Delete)
    http.ListenAndServe(":8080", nil)
}

