BOOKS
-----------
This is a web api to perform basic CRUD operations using Golang and MySQL.

We can Add, View, Update and Delete Book Details using this application.  

MySQL Database
---------------
CREATE DATABASE `books` 

CREATE TABLE `book_details` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `price` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1

Give database credentials in func dbConn() of main.go

RUN The Application
---------------------------------------------------------

To create new go.mod: module sample_crud, run

go mod init sample_crud

To add module requirements and sums, run

go mod tidy

To run web api, run

go run main.go

Server will start on: http://server_ip:port

eg: http://localhost:8080/

or

http://192.168.43.253:8080/

Docker
---------
Using Dockerfile we can build and run docker image of this application.

eg: docker build -t sample_crud:1 .

docker run -it --name sample_crud_1 -p 9000:8080 sample_crud:1

It will expose port number 9000.


